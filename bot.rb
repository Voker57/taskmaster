# encoding: utf-8

=begin
	huick bot
	requires: xmpp4r-simple, activerecord
	:nodoc:
=end
require 'blather/client/client'
require 'optparse'

class TaskmasterBot
	attr_accessor :logger, :client, :commands
	def initialize
		@logger = Minilogger.from_env
		@client = Blather::Client.setup Settings.jid, Settings.password

		@client.register_handler :subscription, :request? do |s|
			@client.write s.approve!
		end
		
		@commands = {}
		@client.register_handler :message do |m|
			next unless m.body
			next if m.from.to_s == Settings.jid # No talking to ourselves
			begin
			Task.transaction do
				
			user = User.assert_registered(m.from.stripped.to_s)
			@logger.info ">#{user.jid} #{m.body.gsub("\n",'\n')}"
			args = Shellwords.split(m.body)
			command = args.shift.downcase.split("/")
			command_name = command.shift
			command_sub = (command.shift || :default).to_sym # TODO: arbitrary levels?
			cmd = @commands[command_name]
			if cmd && cmd.commands.include?(command_sub)
				response = cmd.send(command_sub, user, args)
				say m.from, response
			else
				say m.from, "No such command, try HELP"
			end
			
			end
			rescue Exception => e
				ecode = DateTime.now.strftime("%s")
				say m.from, "Internal error, timestamp #{ecode}."
				@logger.err "[#{DateTime.now.to_s}] {#{ecode}} Exception #{e} raised at #{e.backtrace.join("    ")}"
			end
		end
		
		
	end

	def say(to, msg)
		@client.write Blather::Stanza::Message.new(to, msg)
	end
	
	def run
		@timer = EventMachine::PeriodicTimer.new(1800) do
			say Settings.jid, "PING"
		end
		@nagging_timer = EventMachine::PeriodicTimer.new(60) do
			begin
			Nag.transaction do
			::Nag.where("nagged = :false AND nag_at < :now", false: false, now: DateTime.now).preload(:task).each do |nag|
				# TODO: Replace task finished check with a complicated and efficient JOIN?
				if nag.user && !nag.task.finished
					say nag.user.jid, "Nag from task #{nag.task.id}: #{nag.nag_text}"
				end
				nag.nagged = true
				nag.save!
			end
			end
			rescue Exception => e
				ecode = DateTime.now.strftime("%s")
				say m.from, "Internal error, timestamp #{ecode}."
				@logger.err "[#{DateTime.now.to_s}] {#{ecode}} Exception #{e} raised at #{e.backtrace.join("    ")}"
			end
		end
		@client.run
	end
	
	def add_command(name, klass)
		@commands[name] = klass.new(self)
	end

end

t = TaskmasterBot.new

module Commands
class Auction
	def initialize(bot)
		@bot = bot
	end
	def commands
		@commands ||= Set.new(%w|add show bid finish|.map(&:to_sym))
	end
	def add(user, args)
		options = {}
		parser = OptionParser.new do |opts|
			opts.banner = "Usage: auction/add <text>"
		end
		parser.parse!(args)
		
		return parser.to_s unless args.count == 1
		
		text = args[0]
		auction = ::Auction.new(owner: user, text: text)
		auction.save!
		"Auction added!\n" + auction.render_text
	end
	
	def bid(user, args)
		options = {}
		parser = OptionParser.new do |opts|
			opts.banner = "Usage: auction/bid <auction ID> <text>"
		end
		parser.parse!(args)
		
		return parser.to_s unless args.count == 2
		
		auction = ::Auction.find_by_id(args[0])
		
		return "Couldn't find auction #{args[0]}" unless auction
		return "Auction is finished" if auction.finished
		
		bid = ::Bid.new(auction: auction, owner: user, text: args[1])
		bid.save!
		[auction.owner, *auction.bids.map(&:owner)].uniq.each do |interested_party|
			@bot.say interested_party.jid, "New bid on auction #{auction.id}: [#{bid.id}] #{bid.text}"
		end
		
		"Bid successful!"
	end
	
	def show(user, args)
		options = {}
		parser = OptionParser.new do |opts|
			opts.banner = "Usage: auction/show <auction ID>"
		end
		parser.parse!(args)
		
		return parser.to_s unless args.count == 1
		
		auction = ::Auction.find_by_id(args[0])
		
		return "Couldn't find auction #{args[0]}" unless auction
		
		auction.render_text
	end
	
	def finish(user, args)
		options = {}
		parser = OptionParser.new do |opts|
			opts.on("-w WINNING_BID", "--win WINNING_BID", "These bid(s) won") do |winner|
				(options[:winners] ||= []) << winner
			end
			opts.banner = "Usage: auction/finish <auction ID>"
		end
		parser.parse!(args)
		return parser.to_s unless args.count == 1
		
		auction = ::Auction.find_by_id(args[0])
		
		return "Couldn't find auction #{args[0]}" unless auction
		return "It is not your auction" unless auction.owner == user
		
		(options[:winners] || []).each do |winner_id|
			winner = Bid.find_by_id(winner_id)
			return "Couldn't find bid #{winner_id}" unless winner
			winner.won = true
			winner.save!
		end
		auction.finished = true
		auction.save!
		auction.bids.each do |bid|
			@bot.say(bid.owner.jid, "Auction #{auction.id} finished, your bid #{bid.id} #{bid.won ? "won!" : "lost..."}")
		end
		"Auction #{auction.id} finished!"
	end
end
end

module Commands
class Task
	def initialize(bot)
		@bot = bot
	end
	def commands
		@commands ||= Set.new(%w|add mine finish|.map(&:to_sym))
	end
	def add(user, args)
		options = {}
		parser = OptionParser.new do |opts|
			opts.on("-d DUE", "--due DUE", "When task is due") do |due|
				options[:due] = ::Chronic.parse(due)
			end
			opts.banner = "Usage: task/add <task description> <assignee>"
		end
		parser.parse!(args)
		return parser.to_s.strip if args.count != 2
		
		assignee = ::User.where("jid = :name OR name = :name", name: args[1]).first
		return "Assignee '#{args[1]}' not found!" if !assignee
		t = ::Task.new(owner: user, assignee: assignee, text: args[0], due_by: options[:due])
		# TODO: disableable
		if t.due_by
			t.nag!(t.due_by - 30.minutes, "30 minute warning!")
		end
		t.save!
		
		@bot.say(assignee.jid, "You got a new task:\n#{t.render_text}")
		"Task added!\n" + t.render_text
	end
	def mine(user, args)
		output = []
		output << "Your tasks:"
		::Task.where(assignee: user, finished: false).order("due_by ASC").each do |task|
			output << task.render_text
		end
		output.join("\n")
	end
	
	def finish(user, args)
		options = {}
		parser = OptionParser.new do |opts|
			opts.on("-u", "--undo", "Undo task finishing") do |undo|
				options[:undo] = true
			end
			opts.banner = "Usage: task/finish <task ID>"
		end
		parser.parse!(args)
		return parser.to_s.strip if args.count != 1
		
		task = ::Task.find_by_id(args[0])
		
		return "Task not found" unless task
		return "It is not your task" unless task.owner == user || task.assignee == user
		
		if options[:undo]
			return "Task is not finished" if !task.finished
			task.finished = false
			task.save!
			
		else
			return "Task is finished" if task.finished
			task.finished = true
			task.save!
			"Task marked finished!"
		end
		message = "Task #{task.id} have been marked #{task.finished ? "finished" : "incomplete"}"
		if task.owner != user
			@bot.say(task.owner.jid, message)
		end
		if task.assignee != user
			@bot.say(task.assignee.jid, message)
		end
		message
	end
end
end

t.add_command "task", Commands::Task
t.add_command "auction", Commands::Auction

EventMachine.run do
	

	t.run
end
