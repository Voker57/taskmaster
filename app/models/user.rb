class User < ApplicationRecord
	def self.assert_registered(jid)
		User.find_or_create_by(jid: jid)
	end
	
	def shorthand
		str = "<#{self.jid}>"
		if self.name
			str = self.name + " " + str
		end
		str
	end
end
