class Bid < ApplicationRecord
	belongs_to :auction
	belongs_to :owner, class_name: "User"
end
