class Task < ApplicationRecord
	belongs_to :owner, class_name: "User"
	belongs_to :assignee, class_name: "User"
	
	has_many :nags, dependent: :destroy
	
	def render_text
		ActionController::Base.new.render_to_string(partial: "text/task", locals: { task: self })
	end
	
	def nag!(nag_at, text)
		self.nags << Nag.new(nag_at: nag_at, nag_text: text, user: self.assignee)
	end
end
