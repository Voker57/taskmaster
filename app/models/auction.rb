class Auction < ApplicationRecord
	belongs_to :owner, class_name: "User"
	has_many :bids
	
	def render_text
		ActionController::Base.new.render_to_string(partial: "text/auction", locals: { auction: self })
	end
end
