h1. Taskmaster

Jabber bot which manages tasks.

Distributed under GNU Affero General Public License, see "COPYING":COPYING for details.

h2. Example session

bc. [18:52:39] <Voker57> task/add "demo the bot" voker57@bitcheese.net --due "in 5 minutes" 
[18:52:39] <taskmaster_test@bitcheese.net> You got a new task:
Task 24
Assigned to: <voker57@bitcheese.net>
Created by: <voker57@bitcheese.net>
Due by: 2019-11-16 16:57:39 UTC (in 5 minutes)
[ ] demo the bot
[18:52:39] <taskmaster_test@bitcheese.net> Task added!
Task 24
Assigned to: <voker57@bitcheese.net>
Created by: <voker57@bitcheese.net>
Due by: 2019-11-16 16:57:39 UTC (in 5 minutes)
[ ] demo the bot
[18:53:15] <taskmaster_test@bitcheese.net> Nag from task 24: 30 minute warning! 
[18:53:53] <Voker57> task/finish 24 
[18:55:01] <taskmaster_test@bitcheese.net> Task marked finished! 
[18:55:32] <Voker57> auction/add "improve bot" 
[18:55:32] <taskmaster_test@bitcheese.net> Auction added!
[3] Auction Ongoing
improve bot
Bids:
[18:56:23] <Voker57> auction/bid 3 $500 
[18:56:23] <taskmaster_test@bitcheese.net> New bid on auction 3: $500 
[18:56:23] <taskmaster_test@bitcheese.net> Bid successful! 
[18:57:19] <Voker57> auction/show 3 
[18:57:20] <taskmaster_test@bitcheese.net> Auction [3] Ongoing
improve bot
Bids:
<voker57@bitcheese.net>: [2] $500
[18:57:32] <Voker57> auction/finish --win 2 3 
[18:57:32] <taskmaster_test@bitcheese.net> Auction 3 finished, your bid 2 won! 
[18:57:32] <taskmaster_test@bitcheese.net> Auction 3 finished! 

h2. Deploying

# Install ruby and bundler
# @bundle@
# @cp config/database.yml.example config/database.yml && editor config/database.yml # Postgres connection details@
# @cp config/settings.yml config/settings.local.yml && vim config/settings.local.yml # JID, password, etc@
# rake db:create
# rails r bot.rb
