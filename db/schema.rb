# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_16_161313) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "auctions", force: :cascade do |t|
    t.text "text"
    t.integer "owner_id"
    t.boolean "finished", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["finished"], name: "index_auctions_on_finished"
    t.index ["owner_id"], name: "index_auctions_on_owner_id"
  end

  create_table "bids", force: :cascade do |t|
    t.text "text"
    t.integer "owner_id"
    t.integer "auction_id"
    t.boolean "won"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["auction_id"], name: "index_bids_on_auction_id"
    t.index ["owner_id"], name: "index_bids_on_owner_id"
    t.index ["won"], name: "index_bids_on_won"
  end

  create_table "groups", force: :cascade do |t|
    t.integer "owner_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_id"], name: "index_groups_on_owner_id"
  end

  create_table "nags", force: :cascade do |t|
    t.integer "task_id"
    t.datetime "nag_at"
    t.boolean "nagged", default: false
    t.integer "user_id"
    t.text "nag_text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nag_at"], name: "index_nags_on_nag_at"
    t.index ["nagged"], name: "index_nags_on_nagged"
    t.index ["task_id"], name: "index_nags_on_task_id"
    t.index ["user_id"], name: "index_nags_on_user_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.integer "owner_id"
    t.text "text"
    t.integer "group_id"
    t.datetime "due_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "finished", default: false
    t.integer "assignee_id"
    t.index ["assignee_id"], name: "index_tasks_on_assignee_id"
    t.index ["due_by"], name: "index_tasks_on_due_by"
    t.index ["group_id"], name: "index_tasks_on_group_id"
    t.index ["owner_id"], name: "index_tasks_on_owner_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "jid"
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["jid"], name: "index_users_on_jid", unique: true
    t.index ["token"], name: "index_users_on_token", unique: true
  end

end
