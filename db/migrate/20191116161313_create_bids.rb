class CreateBids < ActiveRecord::Migration[5.2]
  def change
    create_table :bids do |t|
      t.text :text
      t.integer :owner_id
      t.integer :auction_id
      t.boolean :won

      t.timestamps
    end
    add_index :bids, :owner_id
    add_index :bids, :auction_id
    add_index :bids, :won
  end
end
