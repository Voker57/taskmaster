class AddMissingIndexes < ActiveRecord::Migration[5.2]
  def change
	add_index :tasks, :owner_id
	add_index :tasks, :assignee_id
	add_index :tasks, :group_id
	add_index :tasks, :due_by
	
	add_index :users, :jid, unique: true
	add_index :users, :token, unique: true
	
	add_index :groups, :owner_id
  end
end
