class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.integer :owner_id
      t.string :assignee_id
      t.text :text
      t.integer :group_id
      t.datetime :due_by

      t.timestamps
    end
  end
end
