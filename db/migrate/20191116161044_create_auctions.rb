class CreateAuctions < ActiveRecord::Migration[5.2]
  def change
    create_table :auctions do |t|
      t.text :text
      t.integer :owner_id
      t.boolean :finished, default: false
      t.timestamps
    end
    add_index :auctions, :owner_id
    add_index :auctions, :finished
  end
end
