class FixTasksTable < ActiveRecord::Migration[5.2]
  def up
	remove_column :tasks, :assignee_id
	add_column :tasks, :assignee_id, :integer
	add_index :tasks, :assignee_id
  end
  def down
	remove_column :tasks, :assignee_id
	add_column :tasks, :assignee_id, :string
	add_index :tasks, :assignee_id
  end
end
