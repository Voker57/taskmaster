class CreateNags < ActiveRecord::Migration[5.2]
  def change
    create_table :nags do |t|
      t.integer :task_id
      t.datetime :nag_at
      t.boolean :nagged, default: false
      t.integer :user_id
      t.text :nag_text

      t.timestamps
    end
    add_index :nags, :task_id
    add_index :nags, :nag_at
    add_index :nags, :nagged
    add_index :nags, :user_id
  end
end
